package com.demo.helmes.dao;

import java.util.List;

import com.demo.helmes.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

    List<User> findBySessionId(String sessionId);

}
