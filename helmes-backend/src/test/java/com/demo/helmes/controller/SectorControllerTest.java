package com.demo.helmes.controller;

import java.util.List;

import com.demo.helmes.BaseTest;
import com.demo.helmes.entity.Sector;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

public class SectorControllerTest extends BaseTest {

    @Autowired
    private SectorController sectorController;

    private ObjectMapper mapper = new ObjectMapper();

    private final Logger logger = LoggerFactory.getLogger(SectorController.class);

    private final String EXPECTED_RESULT_JSON = "[{\n"
            + "\t\t\"id\": 1,\n"
            + "\t\t\"label\": \"Hello\",\n"
            + "\t\t\"children\": [{\n"
            + "\t\t\t\"id\": 3,\n"
            + "\t\t\t\"label\": \"Kitty\"\n"
            + "\t\t}, {\n"
            + "\t\t\t\"id\": 4,\n"
            + "\t\t\t\"label\": \"There\"\n"
            + "\t\t}]\n"
            + "\t},\n"
            + "\t{\n"
            + "\t\t\"id\": 2,\n"
            + "\t\t\"label\": \"World\",\n"
            + "\t\t\"children\": [{\n"
            + "\t\t\t\"id\": 5,\n"
            + "\t\t\t\"label\": \"Molly\"\n"
            + "\t\t}]\n"
            + "\t}\n"
            + "]";

    @Test
    @Transactional
    public void composesSectorTree() {
        //when
        List<Sector> result = sectorController.getSectorsTree();

        //then
        assertThat(result.size()).isEqualTo(2);

        try {
            String expected = mapper.writeValueAsString(result);
            assertThat(expected).isEqualToIgnoringWhitespace(EXPECTED_RESULT_JSON);
        } catch (Exception ex) {
            logger.error("Failed to prepare fixture");
        }
    }
}
