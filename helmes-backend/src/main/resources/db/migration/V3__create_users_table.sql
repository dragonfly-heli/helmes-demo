create table USER (
  SESSION_ID varchar not null,
  NAME varchar not null,
  TERMS_ACCEPTED boolean
);

create table USER_SECTORS (
  USER_SESSION_ID varchar not null,
  SECTORS_ID int not null
);