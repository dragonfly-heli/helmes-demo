create table SECTOR (
  ID int not null,
  LABEL varchar not null,
  ORDER_ID int not null,
  PARENT_ID varchar
);

create table USER (
  SESSION_ID varchar not null,
  NAME varchar not null,
  TERMS_ACCEPTED boolean
);

create table USER_SECTORS (
  USER_SESSION_ID varchar not null,
  SECTORS_ID int not null
);

INSERT INTO SECTOR (ID, LABEL, PARENT_ID, ORDER_ID) VALUES
  (1,'Hello',NULL,0),
  (2,'World',NULL,1),
  (3,'Kitty',1,0),
  (4,'There',1,1),
  (5,'Molly',2,0);