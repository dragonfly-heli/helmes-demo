# helmes-frontend

A Vue.js project for frontend part of Helmes demo form.

* Used https://auralinna.blog/post/2018/how-to-build-a-complete-form-with-vue-js manual for form
* Used vue-treeselect library for the treeselect element

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
