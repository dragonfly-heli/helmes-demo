package com.demo.helmes.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.demo.helmes.BaseTest;
import com.demo.helmes.dao.UserDao;
import com.demo.helmes.model.UserDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class UserControllerTest extends BaseTest {

    @Autowired
    private UserController userController;

    @Autowired
    private UserDao userDao;

    @Test
    public void addsNewUser() {
        //when
        List<Integer> sectors = new ArrayList<>(Arrays.asList(1,2));
        UserDto userDto = new UserDto("userName", true, sectors, "session1");
        UserDto result = userController.addOrUpdateUser(userDto);

        //then
        assertThat(result).isEqualToComparingFieldByField(userDto);
        assertThat(userDao.findBySessionId("session1").size()).isEqualTo(1);
    }

    @Test
    public void updatesExistingUser() {
        //when
        List<Integer> sectors = new ArrayList<>(Arrays.asList(1,2));
        UserDto userDto1 = new UserDto("userName", true, sectors, "session1");
        userController.addOrUpdateUser(userDto1);

        List<Integer> sectors2 = new ArrayList<>(Arrays.asList(1,2));
        UserDto userDto2 = new UserDto("userName2", false, sectors2, "session1");
        UserDto result = userController.addOrUpdateUser(userDto2);

        //then
        assertThat(result).isEqualToComparingFieldByField(userDto2);
        assertThat(userDao.findBySessionId("session1").size()).isEqualTo(1);
    }


    @Test
    public void addsNewUserWithInvalidSectorsData() {
        //when
        List<Integer> sectors = new ArrayList<>(Arrays.asList(100,200));
        UserDto userDto = new UserDto("userName", true, sectors, "session1");
        UserDto result = userController.addOrUpdateUser(userDto);

        //then
        assertThat(result.getSessionId()).isEqualTo(userDto.getSessionId());
        assertThat(result.getName()).isEqualTo(userDto.getName());
        assertThat(result.getTerms()).isEqualTo(userDto.getTerms());
        assertThat(result.getSectors()).isEmpty();
        assertThat(userDao.findBySessionId("session1").size()).isEqualTo(1);
    }

}
