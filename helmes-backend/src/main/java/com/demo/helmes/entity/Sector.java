package com.demo.helmes.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "SECTOR")
public class Sector {

    @Column(name = "ID")
    @Id
    private Integer id;

    @Column(name = "LABEL", nullable = false)
    private String label;

    @Column(name = "ORDER_ID", nullable = false)
    @JsonIgnore
    private Integer order;

    @ManyToOne
    @JsonIgnore
    private Sector parent;

    @OneToMany(mappedBy="parent")
    @OrderBy("order_id ASC")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Sector> children;

    public Sector(Integer id, String label, Integer order) {
        this.id = id;
        this.label = label;
        this.order = order;
    }

    protected Sector() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<Sector> getChildren() {
        return children;
    }

    public void setChildren(List<Sector> children) {
        this.children = children;
    }

    public Sector getParent() {
        return parent;
    }

    public void setParent(Sector parent) {
        this.parent = parent;
    }
}
