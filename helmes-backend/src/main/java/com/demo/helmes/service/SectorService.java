package com.demo.helmes.service;

import java.util.List;
import java.util.stream.Collectors;

import com.demo.helmes.dao.SectorDao;
import com.demo.helmes.entity.Sector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames={"sectors"})
public class SectorService {

    private SectorDao sectorDao;

    @Autowired
    public SectorService(SectorDao sectorDao) {
        this.sectorDao = sectorDao;
    }

    private final Logger logger = LoggerFactory.getLogger(SectorService.class);

    @Cacheable
    public List<Sector> getSectorsTree() {
        logger.debug("Starting to fetch sectors data tree");
        List<Sector> result = this.sectorDao.findAll().stream()
                    .filter(s -> s.getParent() == null)
                    .collect(Collectors.toList());
        logger.debug("Completed fetching sectors data tree");
        return result;
    }

}