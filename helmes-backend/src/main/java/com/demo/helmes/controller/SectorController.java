package com.demo.helmes.controller;

import java.util.List;

import com.demo.helmes.entity.Sector;
import com.demo.helmes.service.SectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/sector")
public class SectorController {

    private SectorService sectorService;

    @Autowired
    public SectorController(SectorService sectorService) {
        this.sectorService = sectorService;
    }

    private final Logger logger = LoggerFactory.getLogger(SectorController.class);

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://localhost:8081")
    public List<Sector> getSectorsTree() {
        logger.info("Received sectors info request");
        try {
            return sectorService.getSectorsTree();
        } catch (Exception ex) {
            logger.error("Error processing sectors info request, {}", ex.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}