# Helmes

Spring Boot backend for Helmes demo application, using Flyway for DB setup and migrations.

## Run

Runs on ``` http://localhost:8080``` 

H2 DB UI accessible at ``` http://localhost:8080/h2``` 
Username: ``` sa``` , Password: blank