package com.demo.helmes.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User {

    @Column(name = "SESSION_ID")
    @Id
    private String sessionId;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "TERMS_ACCEPTED")
    private Boolean termsAccepted;

    @OneToMany
    private List<Sector> sectors;

    public User() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean termsAccepted() {
        return termsAccepted;
    }

    public void setTermsAccepted(Boolean accepted) {
        this.termsAccepted = accepted;
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }

}
