package com.demo.helmes.model;

import java.util.List;

public class UserDto {

    private String name;

    private Boolean terms;

    private List<Integer> sectors;

    private String sessionId;

    public UserDto() {

    }

    public UserDto(String name, Boolean terms, List<Integer> sectors, String sessionId) {
        this.name = name;
        this.terms = terms;
        this.sectors = sectors;
        this.sessionId = sessionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }

    public List<Integer> getSectors() {
        return sectors;
    }

    public void setSectors(List<Integer> sectors) {
        this.sectors = sectors;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
