import Vue from 'vue'
import Vuelidate from 'vuelidate';
import App from './App'
import VueSession from 'vue-session';

Vue.use(VueSession);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
