import { required, maxLength } from 'vuelidate/lib/validators';
import axios from 'axios';

import Treeselect from '@riophae/vue-treeselect'
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

const FORM_API_UPDATE_URL = "http://localhost:8080/user/update";
const FORM_API_SECTORS_DATA_URL = "http://localhost:8080/sector/all";

export default {
  name: 'app-form',
  components: {
    Treeselect
  },
  data() {
    return {
      isSubmitted: false,
      isError: false,
      isSubmitError: false,
      errors: [],
      submitting: false,
      form: {
        name: '',
        sectors: null,
        terms: false,
      },
      sectorsFormOptions: null
    }
  },
  beforeCreate: function () {
    if (!this.$session.exists()) {
      this.$session.start();
    }
  },
  methods: {
    submit() {
      this.$v.$touch();
      if (!this.$v.$error) {
        this.sendFormData();
      } else {
        this.validationError();
      }
    },
    enableSubmitLoader() {
      this.submitting = true;
    },
    disableSubmitLoader() {
      this.submitting = false;
    },
    getSectorsData({ callback }) {
      axios.get(FORM_API_SECTORS_DATA_URL)
        .then(response => {
          this.sectorsFormOptions = response.data;
          callback();
        })
        .catch(error => {
        });
    },
    sendFormData() {
      this.enableSubmitLoader();
      let payload = this.form;
      payload.sessionId = this.$session.id();

      axios.post(FORM_API_UPDATE_URL, payload).then(response => {
        this.submitSuccess(response);
        this.disableSubmitLoader();
      }).catch(error => {
        console.log('oh noes' + error)
        this.submitError(error);
        this.disableSubmitLoader();
      });
    },
    submitSuccess(response) {
      this.isSubmitted = true;
      this.isError = false;
      this.isSubmitError = false;

      this.form.name = response.data.name;
      this.form.terms = response.data.terms;
      this.form.sectors = response.data.sectors;
    },
    submitError(error) {
      this.isError = true;
      this.isSubmitError = true;
    },
    validationError() {
      this.errors = this.getErrors();
      this.isError = true;
    },
    isErrorField(field) {
      try {
        if (this.getValidationField(field).$error) {
          return true;
        }
      } catch (error) {}

      return this.errors.some(el => el.field === field);
    },
    getErrors() {
      let errors = [];
      for (const field of Object.keys(this.form)) {
        try {
          if (this.getValidationField(field).$error) {
            errors.push({'field': field, 'message': null});
          }
        } catch (error) {}
      }
      return errors;
    },
    getValidationField(field) {
      if (this.$v.form[field]) {
        return this.$v.form[field];
      }
      throw Error('No validation for field ' + field);
    },
    onFieldBlur(field) {
      this.isSubmitted = false;
      try {
        this.getValidationField(field).$touch();
        if (this.getValidationField(field).$error) {
          if (!this.errors.some(el => el.field === field)) {
            this.errors.push({'field': field, 'message': null});
          }
        } else {
          this.errors = this.errors.filter(el => el.field !== field);
        }
      } catch (error) {}
    },
    reload() {
      window.location = '';
    }
  },
  validations: {
    form: {
      name: { required, maxLength: maxLength(100) },
      sectors: { required, maxLength: maxLength(5)},
      terms: { required }
    }
  },
  watch: {
    errors() {
      this.isError = this.errors.length > 0 ? true : false;
    }
  }
}
