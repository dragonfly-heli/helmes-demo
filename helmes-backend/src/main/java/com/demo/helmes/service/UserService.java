package com.demo.helmes.service;

import java.util.List;
import java.util.stream.Collectors;

import com.demo.helmes.dao.SectorDao;
import com.demo.helmes.dao.UserDao;
import com.demo.helmes.entity.Sector;
import com.demo.helmes.entity.User;
import com.demo.helmes.model.UserDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    private UserDao userDao;

    private SectorDao sectorDao;

    @Autowired
    public UserService(UserDao userDao, SectorDao sectorDao) {
        this.userDao = userDao;
        this.sectorDao = sectorDao;
    }

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    public UserDto addOrUpdateUser(UserDto userDto){
        try {
            logger.debug("Starting user update processing for session {} with data {}",
                    userDto.getSessionId(),
                    mapper.writeValueAsString(userDto));
        } catch (JsonProcessingException ex) {
            logger.debug("Starting user update processing for session {}", userDto.getSessionId());
        }

        final User user = mapToUser(userDto);
        this.userDao.save(user);
        final UserDto result = mapToUserDto(user);

        logger.debug("Completed user update processing for session {}", userDto.getSessionId());
        return result;
    }

    private User mapToUser(UserDto userDto) {
        final User user = new User();
        user.setSessionId(userDto.getSessionId());
        user.setName(userDto.getName());
        user.setTermsAccepted(userDto.getTerms());
        user.setSectors(sectorDao.findAllById(userDto.getSectors()));
        return user;
    }

    private UserDto mapToUserDto(User user) {
        final UserDto userDto = new UserDto();
        userDto.setSessionId(user.getSessionId());
        userDto.setName(user.getName());
        userDto.setTerms(user.termsAccepted());
        List<Integer> sectorIds = user.getSectors().stream()
                .map(Sector::getId)
                .collect(Collectors.toList());
        userDto.setSectors(sectorIds);
        return userDto;
    }
}