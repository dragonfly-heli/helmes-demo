package com.demo.helmes.controller;

import com.demo.helmes.model.UserDto;
import com.demo.helmes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/update", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody()
    @CrossOrigin(origins = "http://localhost:8081", methods = {RequestMethod.POST, RequestMethod.OPTIONS})
    public UserDto addOrUpdateUser(@RequestBody UserDto userDto) {
        logger.info("Received user update request for session {}", userDto.getSessionId());
        try {
            return this.userService.addOrUpdateUser(userDto);
        } catch (Exception ex) {
            logger.error("Error processing user update request for session {}, error {}", userDto.getSessionId(), ex.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}