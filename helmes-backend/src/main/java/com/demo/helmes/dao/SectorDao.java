package com.demo.helmes.dao;

import com.demo.helmes.entity.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorDao extends JpaRepository<Sector, Integer> {
}
