package com.demo.helmes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HelmesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelmesApplication.class, args);
	}

}